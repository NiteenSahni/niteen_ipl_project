const csv = require("csv-parser")
const fs = require('fs');
const { connected } = require("process");
function manOfMatch() {
    const results = [];
    let seasonArray = []
    let momObj = {}



    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            // console.log(results)
            for (let index = 0; index < results.length; index++) {
                seasonArray[index] = []
                seasonArray[index].push(results[index].season, results[index].player_of_match)
            }
            for (let index = 0; index < seasonArray.length; index++) {


                if (momObj[seasonArray[index][0]]) {
                    if (momObj[seasonArray[index][0]][seasonArray[index][1]]) {
                        momObj[seasonArray[index][0]][seasonArray[index][1]] += 1
                    }
                    else {
                        momObj[seasonArray[index][0]][seasonArray[index][1]] = 1
                    }
                }
                else {
                    momObj[seasonArray[index][0]] = { [seasonArray[index][1]]: 1 }
                }
            }
            let topBatsMan = {}
            let momArray = []
            console.log(momObj);

            momArray = (Object.entries(momObj)
                .map((info) => Object.entries(info[1]))
                .map((playerInfo) => playerInfo
                    .sort((first, second) => first[1] - second[1])
                    .reverse()).map((data) => data[0]))

            let year = 2008;
            momArray.map((momArrayElement) => {
                topBatsMan[year] = momArrayElement
                year++
            })


            fs.writeFile('../public/output/highest-mom-per-season.json', JSON.stringify(topBatsMan), () => { })



        });
}

manOfMatch()