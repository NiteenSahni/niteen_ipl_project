const csv = require("csv-parser")
const fs = require('fs')
function topEcoBowlers(year) {
    const results = [];
    let deliveryData = []
    let economy = {}
    let matchData = []

    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            fs.createReadStream('../data/deliveries.csv').pipe(csv()).on('data', (data) => {
                deliveryData.push(data)
            })
                .on('end', () => {
                    matchData = results.filter((matchObj) => (matchObj.season == year.toString())).map((obj) => obj.id)
                    deliveryData.map((deliveryInfo) => {
                        if (matchData.includes(deliveryInfo.match_id)) {
                            if (economy[deliveryInfo.bowler]) {
                                if (deliveryInfo.wide_runs == 0 && deliveryInfo.noball_runs == 0) {
                                    economy[deliveryInfo.bowler].numberOfBalls++
                                    economy[deliveryInfo.bowler].runsScored += parseInt(deliveryInfo.batsman_runs) + parseInt(deliveryInfo.extra_runs)
                                }
                                if (deliveryInfo.wide_runs == 1 && deliveryInfo.noball_runs == 0) {

                                    economy[deliveryInfo.bowler].runsScored += parseInt(deliveryInfo.batsman_runs) + parseInt(deliveryInfo.extra_runs)
                                }
                                if (deliveryInfo.wide_runs == 0 && deliveryInfo.noball_runs == 1) {

                                    economy[deliveryInfo.bowler].runsScored += parseInt(deliveryInfo.batsman_runs) + parseInt(deliveryInfo.extra_runs)
                                }

                            }
                            else {

                                if (deliveryInfo.wide_runs == 0 && deliveryInfo.noball_runs == 0 && deliveryInfo.legbye_runs == 0 && deliveryInfo.bye_runs == 0) {
                                    economy[deliveryInfo.bowler] = { numberOfBalls: 1, runsScored: parseInt(deliveryInfo.batsman_runs) + parseInt(deliveryInfo.extra_runs) }
                                }

                                if (deliveryInfo.wide_runs == 0 && deliveryInfo.noball_runs == 0 && deliveryInfo.extra_runs !== 0) {
                                    economy[deliveryInfo.bowler] = { numberOfBalls: 1, runsScored: 0 }

                                }
                            }
                        }
                    })

                    let ecoData = {}

                    Object.entries(economy).map((bowlerInfo) => {

                        ecoData[bowlerInfo[0]] = (((bowlerInfo[1].runsScored) / (bowlerInfo[1].numberOfBalls)) * 6).toFixed(2)
                    })
                    let resultData = (Object.entries(ecoData).sort((a, b) => a[1] - b[1]))
                    resultData = (resultData.slice(0, 10))
                    let resultObj = {}
                    resultObj["Most_economical_Bowlers"] = resultData


                    fs.writeFile('../public/output/top-Economic-Bowlers.json', JSON.stringify(resultObj), () => { })
                })


        })




}
topEcoBowlers(2015)


