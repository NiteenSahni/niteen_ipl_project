const csv = require("csv-parser")
const fs = require('fs')

function matchsPlayerInEachYear() {
    let matchArray = []
    let matchesPerYear = {}
    let results = []

    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            matchArray = results.map((matchObj) => parseInt(matchObj.season))
            matchArray.map((season) => {
                matchesPerYear[season] = matchesPerYear[season] ? matchesPerYear[season] = matchesPerYear[season] + 1 : matchesPerYear[season] = 1

            })


            fs.writeFile('../public/output/1-matches-per-year.json', JSON.stringify(matchesPerYear), () => { })



        });
}
matchsPlayerInEachYear()