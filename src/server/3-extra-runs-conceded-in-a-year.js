const csv = require("csv-parser")
const fs = require('fs')
function extraRunsPerTeam(year) {
    const results = [];
    let deliveryData = []
    let idInfo = []
    let extraRuns = {}

    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            fs.createReadStream('../data/deliveries.csv').pipe(csv()).on('data', (data) => {
                deliveryData.push(data)
            })
                .on('end', () => {

                    idInfo = results.filter((obj) => obj.season == year.toString()).map((a) => a.id)

                    deliveryData.map((matchInfo) => {
                        if (idInfo.includes(matchInfo.match_id)) {
                            extraRuns[matchInfo.bowling_team]

                                ? extraRuns[matchInfo.bowling_team] += parseInt(matchInfo.extra_runs)

                                : extraRuns[matchInfo.bowling_team] = parseInt(matchInfo.extra_runs)

                        }
                    })

                   
                    fs.writeFile('../public/output/extra_runs-per-year.json', JSON.stringify(extraRuns), () => { })


                })

        });

}
extraRunsPerTeam(2016)