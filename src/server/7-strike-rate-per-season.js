const csv = require("csv-parser")
const fs = require('fs')
function strikeRatePerSeason(Batsman) {
    const results = [];
    let deliveryData = []
    let seasonAndId = {}
    let batterStrikeRate = {}

    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            fs.createReadStream('../data/deliveries.csv').pipe(csv()).on('data', (data) => {
                deliveryData.push(data)
            })
                .on('end', () => {
                    results.map((matchObj) => seasonAndId[matchObj.id] = matchObj.season)


                    deliveryData.map((delInfo) => {
                        if (seasonAndId.hasOwnProperty(delInfo.match_id) && delInfo.batsman == Batsman) {
                            if (batterStrikeRate[seasonAndId[delInfo.match_id]]) {
                                if (delInfo.wide_runs == 0) {
                                    batterStrikeRate[seasonAndId[delInfo.match_id]].balls++
                                }
                                batterStrikeRate[seasonAndId[delInfo.match_id]].runs += parseInt(delInfo.batsman_runs)
                            }
                            else {
                                let ball = 0;
                                if (delInfo.wide_runs == 0) {

                                    ball = 1;
                                }
                                batterStrikeRate[seasonAndId[delInfo.match_id]] = { balls: ball, runs: parseInt(delInfo.batsman_runs) }
                            }

                        }

                    })
                  
                    Object.entries(batterStrikeRate).map((info) => {
                        let strikeRate = ((parseInt(info[1].runs) / parseInt(info[1].balls)) * 100).toFixed(2);
                        info[1]["Strike_rate"] = strikeRate;
                    })

                    fs.writeFile('../public/output/strikeRate-per-season.json', JSON.stringify(batterStrikeRate), () => { })

                })


        })



}

strikeRatePerSeason('DA Warner')