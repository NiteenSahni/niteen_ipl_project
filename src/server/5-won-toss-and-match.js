const csv = require("csv-parser")
const fs = require('fs')
function tossAndMatch() {
    const results = [];

    let matchAndTossWinner = {}
    let matchesPerYear = {}


    fs.createReadStream('../data/matches.csv')
        .pipe(csv())
        .on('data', (data) => {
            results.push(data)

        })

        .on('end', () => {
            results.map((matchObj) => {

                if (matchObj.toss_winner == matchObj.winner) {
                    if (matchAndTossWinner[matchObj.toss_winner]) {
                        matchAndTossWinner[matchObj.toss_winner] += 1
                    }
                    else {
                        matchAndTossWinner[matchObj.toss_winner] = 1
                    }
                }




            })




            fs.writeFile('../public/output/won-toss-and-match-per-season.json', JSON.stringify(matchAndTossWinner), () => { })



        });
}
tossAndMatch()