const csv = require("csv-parser")
const fs = require('fs')
function superOverEconomy() {
    const deliveryData = [];

    let superOverData = {}

    fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => {
            deliveryData.push(data)

        })

        .on('end', () => {

            deliveryData.map((matchObj) => {
                if (matchObj.is_super_over == 1) {
                    if (superOverData[matchObj.bowler]) {
                        if (matchObj.wide_runs == 0 && matchObj.noball_runs == 0) {

                            superOverData[matchObj.bowler].numberOfBalls++
                            superOverData[matchObj.bowler].runsScored += parseInt(matchObj.batsman_runs) + parseInt(matchObj.extra_runs)
                        }
                        if (matchObj.wide_runs == 1 && matchObj.noball_runs == 0) {


                            superOverData[matchObj.bowler].runsScored += parseInt(matchObj.batsman_runs) + parseInt(matchObj.extra_runs)
                        }
                        if (matchObj.wide_runs == 0 && matchObj.noball_runs == 1) {


                            superOverData[matchObj.bowler].runsScored += parseInt(matchObj.batsman_runs) + parseInt(matchObj.extra_runs)
                        }


                    }


                    else {
                        if (matchObj.wide_runs == 0 && matchObj.noball_runs == 0 && matchObj.legbye_runs == 0 && matchObj.legbye_runs == 0) {


                            superOverData[matchObj.bowler] = { numberOfBalls: 1, runsScored: parseInt(matchObj.batsman_runs) + parseInt(matchObj.extra_runs) }
                        }
                        if (matchObj.wide_runs == 0 && matchObj.noball_runs == 0 && matchObj.extra_runs !== 0) {


                            superOverData[matchObj.bowler] = { numberOfBalls: 1, runsScored: 0 }
                        }
                    }
                }




            })
            let ecoData = {}
            Object.entries(superOverData).map((bowlerInfo) => {
               
                ecoData[bowlerInfo[0]] = (parseInt(bowlerInfo[1].runsScored) / parseInt(bowlerInfo[1].numberOfBalls)) * 6
            })
            let mostEconomicalBowler = {}
            mostEconomicalBowler["Most_Economical_Bowler_in_Super_Over_is"] = (Object.entries(ecoData).sort((bowler1, bowler2) => bowler1[1] - bowler2[1])[0])


            fs.writeFile('../public/output/most-economical-in-super-over.json', JSON.stringify(mostEconomicalBowler), () => { })

        })
}

superOverEconomy()