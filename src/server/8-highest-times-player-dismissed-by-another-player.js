const csv = require("csv-parser")
const fs = require('fs')
function playerDismissedByAnotherPlayer(batsman) {
    let playerDismissed = {};

    fs.createReadStream('../data/deliveries.csv')
        .pipe(csv())
        .on('data', (data) => {



            if (data.player_dismissed !== "" && data.batsman == batsman) {
                if (playerDismissed[data.player_dismissed] === undefined) {
                    playerDismissed[data.player_dismissed] = {};
                    playerDismissed[data.player_dismissed][data.bowler] = 1;
                } else {

                    if (playerDismissed[data.player_dismissed].hasOwnProperty(data.bowler)) {
                        playerDismissed[data.player_dismissed][data.bowler] = playerDismissed[data.player_dismissed][data.bowler] + 1;
                    } else {
                        playerDismissed[data.player_dismissed][data.bowler] = 1;
                    }
                }
            }
        })
        .on("end", () => {
            let bowlerDismissal = []
            let requiredBowler = []
            Object.entries(playerDismissed).map((playerInfo) => bowlerDismissal.push(playerInfo))
            requiredBowler = (Object.entries(bowlerDismissal[0][1]).sort((a, b) => a[1] - b[1]).reverse()[0])
            let requiredObj = {}
            requiredObj[batsman] = requiredBowler
           

            fs.writeFile('../public/output/player-dissmissed.json', JSON.stringify(requiredObj), () => { })


        })
}



playerDismissedByAnotherPlayer("DA Warner");

